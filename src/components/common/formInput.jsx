import React from 'react';
import TextField from '@material-ui/core/TextField';

export default ({
  input,
  label,
  variant = 'outlined',
  withMargin, 
  fullWidth,
  multiline,
  meta: {
    touched,
    error
  },
  ...custom,
}) => (
    <div className="field-container">
      <TextField
        className="field-container__field"
        label={label}
        variant={variant}
        multiline={multiline}
        fullWidth={fullWidth}
        margin={withMargin ? 'normal' : 'none'}
        {...input}
        rows={multiline ? 5 : undefined}
      />
    </div>
  )