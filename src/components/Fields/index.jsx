import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';

import { Button, withStyles } from '@material-ui/core';
import FormInput from '../common/formInput';
import validate from './validate';

import Paper from '@material-ui/core/Paper';
import Firebase, { messagesRef } from '../../config/firebase';
import { red } from '@material-ui/core/colors';

const styles = ({ spacing: { unit } }) => ({
  root: {
    padding: unit * 5,
  },
  button: {
    width: '20%',
    margin: 'auto !important',
    marginTop: unit * 2,
    padding: `${unit * 1.5}px 0`,
    display: 'block',
  }
});

class Fields extends Component {

  ownSubmitHandler({ author, message }) {
    messagesRef.push().set({
      author,
      message,
      date: Firebase.database.ServerValue.TIMESTAMP
    })
  }

  render() {
    const { handleSubmit, classes } = this.props;

    return (
      <Paper className={classes.root}>
        <form onSubmit={handleSubmit(this.ownSubmitHandler)}>
          <Field
            name="author"
            label="Author"
            component={FormInput}
            fullWidth
          />
          <Field
            name="message"
            label="Message"
            multiline
            component={FormInput}
            fullWidth
            className={null}
            withMargin
          />
          <Button
            variant="outlined"
            color="primary"
            type="submit"
            className={classes.button}
          >
            Send
          </Button>
        </form>
      </Paper>
    )
  }
}

export default withStyles(styles)(reduxForm({
  form: 'fields',
  validate
})(Fields));
