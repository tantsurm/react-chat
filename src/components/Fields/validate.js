export default ({ author, message }) => {
  const errors = {};

  if (!author) errors.author = 'Author is equired!';
  if (!message) errors.message = 'Message is required!';

  return errors;
}