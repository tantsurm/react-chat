import React from 'react';
import { connect } from 'react-redux';

import { getMessagesUpdate } from '../../actions/messages'
// import actions from './../../action';

import Fields from '../Fields';
import MessageBoard from '../MessageBoard';

class Chat extends React.Component {

  render() {
    const { messages: { data } } = this.props;

    return (
      <div className="chat-app">
        <Fields />
        <MessageBoard messages={data} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  messages: state.messages
})
// const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, null)(Chat);
