import React from 'react';
import { withStyles } from '@material-ui/core';

import Message from './message';

// bright one https://htmlcolorcodes.com/assets/images/html-color-codes-color-tutorials-903ea3cb.jpgAS

const styles = theme => ({
  root: {
    overflow: 'auto',
    backgroundImage: 'url("https://mdbootstrap.com/img/Photos/Horizontal/Nature/full%20page/img(11).jpg")',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
  }
});


const MessageBoard = ({ messages, classes }) => (
  messages && messages.length
    ? <div className={classes.root} children={messages.map(message => <Message message={message} />)} />
    : null
);

export default withStyles(styles)(MessageBoard);
