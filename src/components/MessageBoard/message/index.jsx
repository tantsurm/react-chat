import React from 'react';
import { Paper, Typography, withStyles } from '@material-ui/core';

const styles = ({ spacing: { unit } }) => ({
  root: {
    padding: unit * 2,
    margin: unit * 5,
  }
});

const message = ({ message: { message, date, author }, classes }) => (
  <Paper className={classes.root}>
    <Typography variant='h5' component='h3'>{author} at {new Date(date).toLocaleString()}</Typography>
    <Typography component='p' children={message} />
  </Paper>
);

export default withStyles(styles)(message);