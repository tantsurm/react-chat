import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';

import combinedReducers from './../reducers';
import initialState from './initialState';
import objToArray from './../utils/objToArr';

import { getMessagesUpdate } from './../actions/messages';
import { messagesRef } from './../config/firebase';

const logger = createLogger({
  predicate: (getState, action) => !action.type.includes('@@redux-form')
});

const store = createStore(
  combinedReducers,
  initialState,
  applyMiddleware(logger)
)

messagesRef.on(
  'value',
  snapshot => store.dispatch(getMessagesUpdate(objToArray(snapshot.val()).reverse())),
  error => console.warn('read failed', error)
)

export default store;