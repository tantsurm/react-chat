import * as actionTypes from './../../constants';

export const getMessagesUpdate = payload => ({
  type: actionTypes.GET_MESSAGES_UPDATE,
  payload
})