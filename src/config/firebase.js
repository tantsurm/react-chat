import Firebase from 'firebase';

const config = {
  apiKey: "AIzaSyCrfngGT6jUVnEevRUtcVIufERe391C3yk",
  authDomain: "react-chat-60727.firebaseapp.com",
  databaseURL: "https://react-chat-60727.firebaseio.com",
  projectId: "react-chat-60727",
  storageBucket: "react-chat-60727.appspot.com",
  messagingSenderId: "867007901455"
}

Firebase.initializeApp(config);

export default Firebase;

export const database = Firebase.database();
export const messagesRef = database.ref("messages/");