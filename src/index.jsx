import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';

import reduxStore from './state';

import Chat from './components/Chat';

ReactDOM.render(
    <Provider store={reduxStore}>
        <CssBaseline>
            <Chat />
        </CssBaseline>
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
