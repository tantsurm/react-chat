import initialState from './../../state/initialState';
import * as types from './../../constants';

export default function messagesReducer(state = initialState.messages, { type, payload: data, error }) {
  switch (type) {
    case types.GET_MESSAGES_UPDATE: {
      return {
        ...state,
        data
      }
    }
    default: {
      return state;
    }
  }
} 